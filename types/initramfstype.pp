# @summary List of valid initramfs provider types
type Lvmconf::InitramfsType = Enum[
  'dracut',
]
