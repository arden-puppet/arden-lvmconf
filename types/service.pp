# @summary List of valid lvm related service names
type Lvmconf::Service = Enum[
  'metad',
  'lvm',
  'monitor',
]
