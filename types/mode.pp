# @summary List of valid modes for LVM execution
type Lvmconf::Mode = Enum[
  'halvm',
  'cluster',
  'standard',
]
