# @summary Install the lvm package if specified
class lvmconf::install {
  if $lvmconf::package_name {
    package { $lvmconf::package_name:
      ensure => present,
    }
  } else {
    $os_family = $facts['os']['family']
    $os_major_release = $facts['os']['release']['major']
    $message = @("EOT"/L)
      lvmconf::install: package name must be specified when manage_package \
      is enabled on osfamily ${os_family} major release ${os_major_release}!
      |-EOT
    fail($message)
  }
}
