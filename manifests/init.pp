# Ensures that LVM is configured correctly on each 
#
# @summary Performs basic management of LVM in a RedHat environment.
#
# @example Trivial configuration of 'standard' mode
#   include lvmconf
#
# @example Install package and enable HA LVM
#   class { 'lvmconf':
#     systemd        => true,
#     service_map    => {'metad' => 'lvm2-lvmetad'},
#     manage_package => true,
#     mode           => 'halvm',
#     package_name   => 'lvm',
#     local_vgs      => [ 'rootvg', 'nonclustervg' ],
#   }
#
# @param systemd
#   Indicates whether this system uses systemd or sysv as it's init system.
#   Different services will be manipulated depending on the value of this
#   parameter.
#
# @param service_map
#   A hash mapping general names of LVM services to their OS/init system
#   specific variants. Currently only the 'metad' entry is used directly.
#
# @param mode
#   Configures the local LVM instance operation mode. See `man lvmconf` for more
#   detail.
#
# @param manage_package
#   When true puppet will attempt to install the lvm2 package on this host.
#
# @param package_name
#   Name of the LVM2 package for retrieval by the local package manager on the
#   target system. Operating systems which are explicitly supported
#   have hiera configured defaults for this parameter. It is mandatory when
#   `manage_package` is true.
#
# @param manage_initramfs
#   When true the local initramfs will be regenerated to take the changes to
#   lvm.conf into account.
#
# @param initramfs_type
#   Indicates the type of initramfs to be modified. Currently, only dracut is
#   supported by this module.
#
# @param local_vgs
#   Whitelist of local-only volume groups which will be automatically imported
#   by LVM. Each entry in this array will be added to the 
#   'activation/volume_list' array in lvm.conf. This is critical when halvm or
#   clustered LVM are enabled.
#
class lvmconf (
  Boolean $systemd,
  Hash[Lvmconf::Service, String] $service_map = {},
  Lvmconf::Mode $mode                         = 'standard',
  Boolean $manage_package                     = false,
  Optional[String[1]] $package_name           = undef,
  Boolean $manage_initramfs                   = false,
  Lvmconf::InitramfsType $initramfs_type      = 'dracut',
  Array[String] $local_vgs                    = [],
) {
  if $lvmconf::manage_package {
    contain 'lvmconf::install'
    Class['lvmconf::install']
    -> Class['lvmconf::config']
  }

  contain 'lvmconf::config'
  contain 'lvmconf::service'

  Class['lvmconf::config']
  ~> Class['lvmconf::service']
}
