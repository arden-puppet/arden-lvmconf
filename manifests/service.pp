# @summary Manage the state of each LVM related service based on lvmconf mode
class lvmconf::service {
  # Determine the platform we're running on
  $os_family = $facts['os']['family']
  $os_major_release = $facts['os']['release']['major']
  case $os_family {
    'RedHat': {
      if Integer($os_major_release) > 7 {
        $metad_exists = false
      } else {
        $metad_exists = true
      }
    }
    default: {
      $metad_exists = true
    }
  }

  if $metad_exists {
    # Only metad is necessary right now
    unless('metad' in $lvmconf::service_map) {
      $message = @("EOT"/L)
        lvmconf::service: mapping for lvm metad service missing on \
        osfamily ${os_family} major release ${os_major_release}!
        |-EOT
      fail($message)
    }
    $metad_service = $lvmconf::service_map['metad']

    case $lvmconf::mode {
      'standard': {
        # Start the main service
        service { $metad_service:
          ensure => running,
          enable => true,
        }
        # We need to ensure the metad socket isn't still masked on systemd
        # See https://access.redhat.com/solutions/3019511 for detail
        if $lvmconf::systemd {
          service { "${metad_service}.socket":
            ensure => running,
            enable => true,
          }
        }
      }
      'halvm', 'cluster': {
        service { $metad_service:
          ensure => stopped,
          enable => false,
        }
        # On systemd we need to mask the socket
        if $lvmconf::systemd {
          service { "${metad_service}.socket":
            ensure => stopped,
            enable => 'mask',
          }
        }
      }
      default: {}
    }
  }

  # Regenerate the initramfs if configuration changes were detected
  if $lvmconf::manage_initramfs {
    case $lvmconf::initramfs_type {
      'dracut': {
        exec { 'regenerate_initramfs':
          command     => 'dracut --force --add lvm',
          path        => '/sbin:/bin:/usr/sbin:/usr/bin',
          refreshonly => true,
        }
      }
      default: {
        fail("lvmconf::serice: Unsupported initramfs type ${lvmconf::initramfs_type}!")
      }
    }
  }
}
