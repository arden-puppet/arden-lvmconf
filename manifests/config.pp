# @summary Enforces lvm.conf based on lvmconf mode flags
class lvmconf::config {
  # Ensure our whitelist is populated appropriately
  if ! empty($lvmconf::local_vgs) {
    $volume_string = join($lvmconf::local_vgs, '","')
    lvm_config { 'activation/volume_list':
      value => "[\"${volume_string}\"]",
    }
  } elsif $lvmconf::mode != 'standard' {
    fail("lvmconf::config: A volume list must be specified when mode ${lvmconf::mode} is enabled!")
  }
  # TODO: handle clearing the volume list

  # lvmetad was removed post RHEL 7 and both modes are managed via the same
  # mechanism
  if Integer($facts['os']['release']['major']) < 8 {
    # Configure locking mode
    case $lvmconf::mode {
      'standard','halvm': {
        lvm_config { 'global/locking_type':
          value => '1',
        }
      }
      'cluster': {
        if Integer($facts['os']['release']['major']) > 7 {
          $locking_type_value = '1'
        } else {
          $locking_type_value = '3'
        }
        lvm_config { 'global/locking_type':
          value => $locking_type_value,
        }
      }
      default: {}
    }
    # Configure lvmetad
    case $lvmconf::mode {
      'standard': {
        lvm_config { 'global/use_lvmetad':
          value => '1',
        }
      }
      'halvm', 'cluster': {
        lvm_config { 'global/use_lvmetad':
          value => '0',
        }
      }
      default: {}
    }
  }
}
