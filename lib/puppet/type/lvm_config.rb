# frozen_string_literal: true

require 'puppet/parameter/boolean'

Puppet::Type.newtype(:lvm_config) do
  @doc = "Type to manage entries in the `/etc/lvm/lvm.conf` config file via
    the lvmconfig (aka lvm config) command.

    Example:

    ```puppet
    lvm_config { 'activation/volume_list':
      value  => [\"vg1\",\"vg2\"],
    }
    ```"

  newparam(:name) do
    desc "The Section/Setting key string which corresponds to the desired
      variable. See the output of `lvmconfig -l` for reference. Values are
      not validated, however, the keys themselves are tested prior to creating
      the entry."

    validate do |value|
      unless %r{^[a-z]+[/][a-z0-9_]+$}.match?(value)
        raise Puppet::Error, "Puppet::Type::Lvm_Config: property '#{value}' appears to be invalid!"
      end
    end

    isnamevar
  end

  newparam(:include_comments, boolean: true, parent: Puppet::Parameter::Boolean) do
    desc "When true full descriptive comments will be included in the
      resulting lvm config file. Note that currently this is all or nothing. If
      you want to ensure comments are not included make sure you disable this
      for every instance of lvm_config!"

    defaultto :true
  end

  newproperty(:value) do
    desc "Data to be stored as the value for the specified
      Section/Setting key. Some validation is performed by lvmconfig
      during the apply process. If an error can be detected the provider
      will also generate a failure during the apply."

    validate do |value|
      unless value.is_a? String
        raise ArgumentError, "Puppet::Type::Lvm_conifg: value property must be a string, not a #{value.class}!"
      end
    end

    # Ensure
    def insync?(is)
      is_to_s(is) == should_to_s(should)
    end

    # All spaces are removed for comparison purposes
    def should_to_s(value = @should)
      if value
        # Sometimes values are arrays
        temp = if value.is_a? Array
                 value[0]
               else
                 value
               end
        temp.gsub(%r{\s+}, '')
      else
        nil
      end
    end

    # All spaces are removed for comparison purposes
    # rubocop:disable Style/PredicateName
    def is_to_s(value = @is)
      # rubocop:enable Style/PredicateName
      if value
        # Sometimes values are arrays
        temp = if value.is_a? Array
                 value[0]
               else
                 value
               end
        temp.gsub(%r{\s+}, '')
      else
        nil
      end
    end
  end
end
