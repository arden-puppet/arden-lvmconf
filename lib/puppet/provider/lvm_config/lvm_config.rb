# frozen_string_literal: true

require 'pathname'
require 'base64'
require 'pp'

Puppet::Type.type(:lvm_config).provide(:lvm_config) do
  desc 'Base provider for attributes within the standard Linux LVM2
          `/etc/lvm/lvm.conf` file. Note that this management is achived
          via the utility `lvmconfig`.'

  commands lvmconfig: '/sbin/lvmconfig'

  mk_resource_methods

  defaultfor osfamily: [:redhat, :gentoo]

  #------------- Class Methods ------------------------------------------------
  # Any method in this section must be called via self.class.<method> from an
  # instance level method.

  # Executes the provided command with some sane defaults.
  def self._run_command(cmd,
                        failonfail = true,
                        custom_environment = { combine: true })

    # TODO: Potentially add some handling for when failonfail is false
    raw = Puppet::Util::Execution.execute(
      cmd,
      { failonfail: failonfail }.merge(custom_environment),
    )
    status = raw.exitstatus
    { raw: raw, status: status } if status.zero? || failonfail == false
  end

  # Retrieve the current value
  def self.get_config_value(setting_key, default_value = false)
    query_type = default_value ? 'default' : 'current'
    cmd = [command(:lvmconfig), '--type', query_type, setting_key]

    result = _run_command(cmd, false)
    # Return the current value or a blank
    if result[:status].zero?
      unless %r{\w+=(?<value>[[:print:]]+)} =~ result[:raw]
        value = 'no match'
      end
    else
      value = 'error'
    end

    debug("key: #{setting_key} value: #{value}")
    { value: value, status: result[:status] }
  end

  # ======== Global functions
  # Build a total list of parameters
  def self.section_setting_keys
    cmd = [command(:lvmconfig), '--type', 'list', '--ignorelocal']
    result = _run_command(cmd)

    keys = result[:raw].split(%r{\n+})

    # Prune unsupported keys (currently just tag
    keys.reject { |entry| %r{tag}.match(entry) }
  end

  # Determine the value of the provided key and construct an instance
  def self.construct_instance(setting_key)
    current_value = get_config_value(setting_key)
    default_value = get_config_value(setting_key, true)

    value = if current_value[:status].zero?
              current_value[:value]
            else
              default_value[:value]
            end

    {
      name:          setting_key,
      ensure:        :present,
      value:         value,
      default_value: default_value[:value],
    }
  end

  # Collect the current values for each possible parameter at initialization
  def self.instances
    # Get the full listing of values
    setting_keys = section_setting_keys

    # Initialize the values for each key (unless they're not set)
    instances = []
    setting_keys.each do |setting|
      instance_data = construct_instance(setting)
      instances << new(instance_data)
    end

    instances
  end

  def self.prefetch(resources)
    lvm_config_keys = instances
    lvm_config_keys.each do |provider|
      # rubocop:disable Lint/AssignmentInCondition
      if res = resources[provider.name.to_s]
        # rubocop:enable Lint/AssignmentInCondition
        res.provider = provider
      end
    end
  end

  def self._header_text
    <<~HEREDOC
      # Managed by puppet: lvm_config
      # Do not modify this file directly, changes will be overwritten!
    HEREDOC
  end

  def self.write_lvm_conf(output_data)
    # Prepend the header
    lvm_conf_data = _header_text.dup
    lvm_conf_data.concat(output_data)

    # Write the combined output
    lvm_conf_path = Pathname.new('/etc/lvm/lvm.conf')
    lvm_conf_path.open('w') do |file|
      file << lvm_conf_data
    end
  end

  #------------- Instance Methods ---------------------------------------------
  # Methods in this section operate on the instance level.
  # Flush all changes to config. This method is triggered once the catalog is
  # fully compiled for each instance which had changes to its property_hash
  def flush
    return if @property_hash.empty?

    # Validate the desired directive. Note that this should fail if the value is
    # malformed
    key_name = @property_hash[:name]
    key_value = @property_hash[:value]
    config_entry = "#{key_name}=#{key_value}"
    cmd = [command(:lvmconfig), '--config', config_entry, '--validate']
    self.class._run_command(cmd)

    # Capture the configuration string merged with existing config values
    cmd = [
      command(:lvmconfig),
      '--type',
      'current',
      '--mergedconfig',
      '--config',
      config_entry,
      '--ignorelocal',
    ]
    if @resource[:include_comments]
      cmd << '--withcomments'
    end
    result = self.class._run_command(cmd)

    # Update lvmconf
    self.class.write_lvm_conf(result[:raw])
  end
end
