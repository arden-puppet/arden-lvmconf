# Changelog

All notable changes to this project will be documented in this file.

## 0.1.1 (2023-07-03)

### Feature (1 change)

- [feat: redhat 8 & 9 support](arden-puppet/arden-lvmconf@2a0d49c580f0ea85bb708cf3aca38a2542bc130f) ([merge request](arden-puppet/arden-lvmconf!1))

### Maintenance (1 change)

- [maint: pdk framework update](arden-puppet/arden-lvmconf@f76c10a40228027b7d44f7731a024eb4acb9af08) ([merge request](arden-puppet/arden-lvmconf!1))
