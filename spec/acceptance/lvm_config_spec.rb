# frozen_string_literal: true

require 'spec_helper_acceptance'

describe 'lvm_config type and provider' do
  let(:manifest) do
    <<-EOS
      class { 'lvmconf':
        manage_package => true,
      }
    EOS
  end

  it 'apply without errors' do
    apply_manifest(manifest, expect_changes: true, debug: false, trace: true)
  end

  describe package('lvm2') do
    it 'installs lvm2' do
      is_expected.to be_installed
    end
  end

  it 'second run triggers no changes' do
    result = apply_manifest(manifest, expect_changes: false, debug: false, trace: true)
    expect(result.exit_code).to eq 0
  end

  context 'bad config key' do
    let(:manifest) do
      <<-EOS
        class { 'lvmconf':
          manage_package => true,
        }

        lvm_config { 'bad-config-target':
          value   => 'garbage',
          require => Class['lvmconf'],
        }
      EOS
    end

    it 'rejects invalid config key names' do
      apply_manifest(manifest, expect_failures: true, debug: false, trace: true) do |result|
        expect(result.stderr).to match(%r{Puppet::Type::Lvm_Config: property 'bad-config-target' appears to be invalid!})
      end
    end
  end

  context 'bad config value' do
    let(:manifest) do
      <<-EOS
        class { 'lvmconf':
          manage_package => true,
        }

        lvm_config { 'activation/udev_rules':
          value   => 'garbage',
          require => Class['lvmconf'],
        }
      EOS
    end

    it 'rejects invalid config key names' do
      apply_manifest(manifest, expect_failures: true, debug: false, trace: true) do |result|
        expect(result.stderr).to match(%r{Configuration setting "activation/udev_rules" invalid. Found string value "garbage", expected boolean value: 0/1, "y/n", "yes/no", "on/off", "true/false"})
      end
    end
  end

  context 'setting a parameter to its default has no effect' do
    let(:manifest) do
      <<-EOS
        class { 'lvmconf':
          manage_package => true,
        }

        lvm_config { 'activation/udev_rules':
          value   => '1',
          require => Class['lvmconf'],
        }
      EOS
    end

    it 'detects no changes' do
      apply_manifest(manifest, catch_changes: true, debug: false, trace: true)
    end
  end

  context 'changing a parameter updates the physical lvm.conf file' do
    let(:manifest) do
      <<-EOS
        class { 'lvmconf':
          manage_package => true,
        }

        lvm_config { 'activation/udev_rules':
          value   => '0',
          require => Class['lvmconf'],
        }
      EOS
    end

    it 'detects changes' do
      apply_manifest(manifest, expect_changes: true, debug: false, trace: true)
    end

    it 'updates lvmconf to reflect the change' do
      shell('/sbin/lvmconfig activation/udev_rules') do |result|
        expect(result.stdout).to match(%r{udev_rules=0})
      end
    end

    it 'prepends the warning header to lvm.conf' do
      shell('/bin/head -n 2 /etc/lvm/lvm.conf') do |result|
        expect(result.stdout).to match(%r{^# Managed by puppet: lvm_config})
        expect(result.stdout).to match(%r{^# Do not modify this file directly, changes will be overwritten!})
      end
    end
  end

  context 'configuring multiple parameters' do
    let(:manifest) do
      <<-EOS
        lvm_config { 'devices/pv_min_size':
          value   => '4096',
        }

        lvm_config { 'activation/volume_list':
          value   => '[ "vg1", "vg2" ]',
        }
      EOS
    end

    it 'applies both changes' do
      apply_manifest(manifest, expect_changes: true, debug: false, trace: true)
      apply_manifest(manifest, catch_changes: true, debug: false, trace: true)

      shell('/sbin/lvmconfig devices/pv_min_size activation/volume_list') do |result|
        expect(result.stdout).to match(%r{pv_min_size=4096})
        expect(result.stdout).to match(%r{volume_list=\["vg1","vg2"\]})
      end
    end
  end

  context 'disabling comments' do
    let(:manifest) do
      <<-EOS
        Lvm_config { include_comments => false }

        lvm_config { 'devices/pv_min_size':
          value   => '4096',
        }

        lvm_config { 'activation/volume_list':
          value   => '[ "vg1", "vg2", "vg3" ]',
        }
      EOS
    end

    it 'only includes the header warning comment in lvm.conf' do
      apply_manifest(manifest, expect_changes: true, debug: false, trace: true)
      apply_manifest(manifest, catch_changes: true, debug: false, trace: true)

      shell('/bin/grep "^#" /etc/lvm/lvm.conf | wc -l') do |result|
        expect(result.stdout).to match(%r{2})
      end
    end
  end
end
