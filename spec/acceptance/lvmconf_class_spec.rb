# frozen_string_literal: true

require 'spec_helper_acceptance'

describe 'lvmconf class' do
  context 'standard config with package management' do
    let(:manifest) do
      <<-EOS
        class { 'lvmconf':
          manage_package => true,
        }
      EOS
    end

    it 'applies without errors and is idempotent on the second run' do
      apply_manifest(manifest, catch_failures: true, debug: false, trace: true)
      apply_manifest(manifest, catch_changes: true, debug: false, trace: true)
    end

    describe package('lvm2') do
      it { is_expected.to be_installed }
    end

    if os[:family] == 'RedHat' && os[:release].to_i < 8
      describe service('lvm2-lvmetad') do
        it { is_expected.to be_enabled }
        it { is_expected.to be_running }
      end

      describe service('lvm2-lvmetad.socket') do
        it { is_expected.to be_enabled }
        it { is_expected.to be_running }
      end

      it 'lvm standard locking and lvmetad are both enabled' do
        shell('/sbin/lvmconfig global/use_lvmetad global/locking_type') do |result|
          expect(result.stdout).to match(%r{use_lvmetad=1})
          expect(result.stdout).to match(%r{locking_type=1})
        end
      end
    end

    it 'does not generate the initramfs after updating lvm.conf' do
      shell('/bin/test "/boot/initramfs-$(uname -r).img" -nt /etc/lvm/lvm.conf; echo $?') do |result|
        expect(result.stdout).to match(%r{^1$})
      end
    end
  end

  context 'halvm config with package management and initramfs regeneration' do
    let(:manifest) do
      <<-EOS
        class { 'lvmconf':
          mode             => 'halvm',
          manage_package   => true,
          manage_initramfs => true,
          local_vgs        => [ 'rootvg', 'othervg' ],
        }
      EOS
    end

    it 'applies without errors and is idempotent on the second run' do
      apply_manifest(manifest, catch_failures: true, debug: false, trace: true)
      apply_manifest(manifest, catch_changes: true, debug: false, trace: true)
    end

    describe package('lvm2') do
      it { is_expected.to be_installed }
    end

    # It would make sense to check if lvm2-lvmetad service is disabled here but
    # apparently masking the socket causes the service to be stopped but
    # 'enabled'
    if os[:family] == 'RedHat' && os[:release].to_i < 8
      describe service('lvm2-lvmetad') do
        it { is_expected.not_to be_running }
      end

      describe service('lvm2-lvmetad.socket') do
        it { is_expected.not_to be_enabled }
        it { is_expected.not_to be_running }
      end

      it 'lvm standard locking is enabled and lvmetad is disabled' do
        shell('/sbin/lvmconfig global/use_lvmetad global/locking_type') do |result|
          expect(result.stdout).to match(%r{use_lvmetad=0})
          expect(result.stdout).to match(%r{locking_type=1})
        end
      end
    end

    it 'generates the initramfs after updating lvm.conf' do
      shell('/bin/test "/boot/initramfs-$(uname -r).img" -nt /etc/lvm/lvm.conf; echo $?') do |result|
        expect(result.stdout).to match(%r{^0$})
      end
    end
  end
end
