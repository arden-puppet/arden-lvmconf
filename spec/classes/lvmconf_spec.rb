# frozen_string_literal: true

require 'spec_helper'
require 'deep_merge'

describe 'lvmconf' do
  context 'without hiera data' do
    let(:facts) do
      {
        os: {
          family: 'weird-os',
          release: {
            major: '2000',
          },
        },
      }
    end
    let(:params) do
      {
        manage_package: true,
        systemd: true,
      }
    end

    it 'install fails without package name' do
      is_expected.to compile.and_raise_error(%r{lvmconf::install: package name must be specified when manage_package is enabled on osfamily weird-os major release 2000!})
    end

    it 'fails service configuration without metad mapping' do
      params[:package_name] = 'lvm2'
      is_expected.to compile.and_raise_error(%r{lvmconf::service: mapping for lvm metad service missing on osfamily weird-os major release 2000!})
    end
  end

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:params) do
        {
          manage_package: false,
        }
      end

      before(:each) do
        Puppet::Type.type(:service).defaultprovider.has_feature :maskable if facts[:osfamily] == 'RedHat'
        facts.deep_merge!(
          os: {
            family: facts[:osfamily],
            name: facts[:operatingsystem],
            release: {
              major: facts[:operatingsystemmajrelease],
            },
          },
        )
      end

      it 'compiles with default parameters' do
        is_expected.to compile
      end

      it 'contains the correct classes and enforces their ordering' do
        is_expected.not_to contain_class('lvmconf::install')
        is_expected.to contain_class('lvmconf::config').with(
          notify: ['Class[Lvmconf::Service]'],
        )
        is_expected.to contain_class('lvmconf::service')
      end

      it 'does not regenerate the initramfs by default' do
        is_expected.not_to contain_exec('regenerate_initramfs')
      end

      it 'does not manage the package by default' do
        package_name = case facts[:osfamily]
                       when 'RedHat'
                         'lvm2'
                       else
                         'os-family-should-be-unsupported'
                       end
        is_expected.not_to contain_package(package_name)
      end

      context 'package management enabled' do
        before(:each) do
          params[:manage_package] = true
        end

        it 'compiles with the package enabled' do
          is_expected.to compile
        end

        it 'contains the correct classes and enforces their ordering' do
          is_expected.to contain_class('lvmconf::install').with(
            before: ['Class[Lvmconf::Config]'],
          )
        end

        it 'installs the correct package' do
          params[:manage_package] = true
          package_name = case facts[:osfamily]
                         when 'RedHat'
                           'lvm2'
                         else
                           'os-family-should-be-unsupported'
                         end
          is_expected.to contain_package(package_name).with(
            ensure: 'present',
          )
        end
      end

      context 'initramfs management enabled' do
        before(:each) do
          params.merge!(
            manage_initramfs: true,
            initramfs_type: 'dracut',
          )
        end

        it 'compiles as expected' do
          is_expected.to compile
        end

        it 'regenerates the initramfs' do
          is_expected.to contain_exec('regenerate_initramfs').with(
            command: 'dracut --force --add lvm',
            path: '/sbin:/bin:/usr/sbin:/usr/bin',
            refreshonly: true,
          )
        end
      end

      context 'standard mode' do
        if os_facts[:osfamily] == 'RedHat' && os_facts[:operatingsystemmajrelease].to_i < 8
          it 'enables lvm metad services in standard mode' do
            metad_name = case facts[:osfamily]
                         when 'RedHat'
                           case facts[:os][:release][:major]
                           when '7'
                             'lvm2-lvmetad'
                           else
                             'os-release-should-be-unsupported'
                           end
                         else
                           'os-family-should-be-unsupported'
                         end
            is_expected.to contain_service(metad_name).with(
              ensure: 'running',
              enable: true,
            )
            is_expected.to contain_service("#{metad_name}.socket").with(
              ensure: 'running',
              enable: true,
            )
          end

          it 'configures standard locking and instructs lvm to use metad' do
            is_expected.to contain_lvm_config('global/use_lvmetad').with(
              value: '1',
            )
            is_expected.to contain_lvm_config('global/locking_type').with(
              value: '1',
            )
          end
        elsif os_facts[:osfamily] == 'RedHat' && os_facts[:operatingsystemmajrelease].to_i >= 8
          it 'does not reference lvmetad' do
            is_expected.not_to contain_service('lvm2-lvmetad')
          end

          it 'does not attempt to update global/use_lmetad' do
            is_expected.not_to contain_lvm_config('global/use_lvmetad')
          end

          it 'does not ensure global/locking_type is set to 1' do
            is_expected.not_to contain_lvm_config('global/locking_type')
          end
        end
      end

      context 'halvm mode' do
        before(:each) do
          params.merge!(
            mode: 'halvm',
            local_vgs: ['rootvg', 'appvg'],
          )
        end

        it 'compiles with valid arguments' do
          is_expected.to compile
        end

        it 'fails when an activation volume list is omitted' do
          params.delete(:local_vgs)
          is_expected.to compile.and_raise_error(%r{lvmconf::config: A volume list must be specified when mode halvm is enabled!})
        end

        if os_facts[:osfamily] == 'RedHat' && os_facts[:operatingsystemmajrelease].to_i < 8
          it 'disables the lvm metad service' do
            metad_name = case facts[:osfamily]
                         when 'RedHat'
                           case facts[:os][:release][:major]
                           when '7'
                             'lvm2-lvmetad'
                           else
                             'os-release-should-be-unsupported'
                           end
                         else
                           'os-family-should-be-unsupported'
                         end
            is_expected.to contain_service(metad_name).with(
              ensure: 'stopped',
              enable: false,
            )
            is_expected.to contain_service("#{metad_name}.socket").with(
              ensure: 'stopped',
              enable: 'mask',
            )
          end

          it 'enables lvm locking and instructs lvm not to use lvmetad' do
            is_expected.to contain_lvm_config('global/use_lvmetad').with(
              value: '0',
            )
            is_expected.to contain_lvm_config('global/locking_type').with(
              value: '1',
            )
          end
        elsif os_facts[:osfamily] == 'RedHat' && os_facts[:operatingsystemmajrelease].to_i >= 8
          it 'does not reference lvmetad' do
            is_expected.not_to contain_service('lvm2-lvmetad')
          end

          it 'does not attempt to update global/use_lmetad' do
            is_expected.not_to contain_lvm_config('global/use_lvmetad')
          end

          it 'does not ensure global/locking_type is set to 1' do
            is_expected.not_to contain_lvm_config('global/locking_type')
          end
        end

        it 'configures the provided activation volume list' do
          is_expected.to contain_lvm_config('activation/volume_list').with(
            value: '["rootvg","appvg"]',
          )
        end
      end
      # TODO: implement tests for 'cluster' mode
    end
  end
end
